<?php

namespace MediaWiki\Extension\StructuredBlockReasons\EditorCapabilities;

use MediaWiki\Extension\CommunityConfiguration\EditorCapabilities\AbstractEditorCapability;

class BlockCategoriesEditorCapability extends AbstractEditorCapability {

	/**
	 * @inheritDoc
	 */
	public function execute( ?string $subpage ): void {

	}
}

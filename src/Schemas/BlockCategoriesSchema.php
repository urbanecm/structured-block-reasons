<?php

namespace MediaWiki\Extension\StructuredBlockReasons\Schemas;

use MediaWiki\Extension\CommunityConfiguration\Schema\JsonSchema;

// phpcs:disable Generic.NamingConventions.UpperCaseConstantName.ClassConstantNotUpperCase
class BlockCategoriesSchema extends JsonSchema {

	public const Categories = [
		self::TYPE => self::TYPE_OBJECT,
		self::ADDITIONAL_PROPERTIES => [
			'name' => [
				self::TYPE => self::TYPE_STRING,
			]
		],
		self::DYNAMIC_DEFAULT => true,
	];

	public static function getDefaultCategories() {
		return (object)[];
	}
}

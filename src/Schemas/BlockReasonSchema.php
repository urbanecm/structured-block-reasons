<?php

namespace MediaWiki\Extension\StructuredBlockReasons\Schemas;

use MediaWiki\Extension\CommunityConfiguration\Schema\JsonSchema;

class BlockReasonSchema extends JsonSchema {

	public const reasons = [
		self::TYPE => self::TYPE_ARRAY,
		self::ITEMS => [
			self::TYPE => self::TYPE_OBJECT,
			self::PROPERTIES => [

			],
			self::ADDITIONAL_PROPERTIES => false,
		]
	];
}
